import pandas as pd
import numpy as np
import matplotlib


def get_model_dataset():
    #kinigrnihwuhufheifi
    sales_data = pd.read_csv("model_data.csv")
    sales_data = sales_data[sales_data['DMND_CHNL']  == 'WO']
    sales_data = sales_data[
        ['Coupon Tiers', 'WK_START_DT', 'QUANTITY_SOLD', 'RETAIL_SALES', 'FINAL_SALES', 'SEASONALITY_FLAG_M1',
         'SEASONALITY_FLAG_M2', 'SEASONALITY_FLAG_M3', 'SEASONALITY_FLAG_M4', 'SEASONALITY_FLAG_M5',
         'SEASONALITY_FLAG_M6', 'SEASONALITY_FLAG_M7', 'SEASONALITY_FLAG_M8', 'SEASONALITY_FLAG_M9',
         'SEASONALITY_FLAG_M10',
         'SEASONALITY_FLAG_M11', 'SEASONALITY_FLAG_M12', 'YEAR_TREND', 'BACK_TO_SCHOOL',
         'MEMORIAL_DAY', 'END_OF_SEASON_SALE', 'BLACK_FRIDAY_SALE_IN_JULY', 'MOTHERS_DAY', 'FATHERS_DAY_SALE',
         'ANNIVERSARY_SALE',
         'BELK_DAYS_SALE', 'PRE_THANKSGIVING', 'THANKSGIVING_SALE', 'COLUMBUS_DAY', 'BLACK_FRIDAY_SALE', 'CYBER_SALE',
         'EASTER_SALE',
         'PRE_CHRISTMAS', 'CHRISTMAS_SALE', 'POST_CHRISTMAS', 'VALENTINES_SALE', 'CHARITY_DAY', 'VET_DAY',
         'PRESIDENTS_DAY', 'INDEPENDENCE_DAY', 'LABOUR_DAY',
         'MLK_DAY', 'DMND_CHNL']]

    sales_data['PERCENT_OFF'] = (sales_data['RETAIL_SALES'] - sales_data['FINAL_SALES']) / sales_data['RETAIL_SALES']
    sales_data = sales_data.drop(['RETAIL_SALES', 'FINAL_SALES'], axis=1)
    return sales_data


def backward_elimination(data, target, significance_level=0.05):
    features = data.columns.tolist()
    while (len(features) > 0):
        features_with_constant = sm.add_constant(data[features])
        p_values = sm.OLS(target, features_with_constant).fit().pvalues[1:]
        max_p_value = p_values.max()
        if (max_p_value >= significance_level):
            excluded_feature = p_values.idxmax()
            features.remove(excluded_feature)
        else:
            break
    return features